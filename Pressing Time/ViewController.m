//
//  ViewController.m
//  Pressing Time
//
//  Created by Luu Nguyen on 5/3/16.
//  Copyright © 2016 Uphave. All rights reserved.
//

#import "ViewController.h"
#import "PressingView.h"

@interface ViewController () <PressingViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *txtTimer;
@property (weak, nonatomic) IBOutlet PressingView *pressingView;

@end

@implementation ViewController {
    NSTimer *_countTimer;
    unsigned int _count;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_pressingView setDelegate:self];
    [_txtTimer setHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touched {
    [_txtTimer setHidden:YES];
}

- (void)pressed {
    [_txtTimer setHidden:NO];
    [_countTimer invalidate];
    _count = [[NSDate date] timeIntervalSince1970];
    _countTimer = [NSTimer scheduledTimerWithTimeInterval:0.01
                                                    target:self
                                                  selector:@selector(updateTimer)
                                                  userInfo:nil
                                                  repeats:YES];
}

- (void)released {
    [_countTimer invalidate];
}

- (void)updateTimer {
    int time = ([[NSDate date] timeIntervalSince1970] - _count) * 1000;
    int minute = time / 1000 / 60;
    int second = (time / 1000) % 60;
    int milli = (time % 1000) / 10;
    NSString *txt = [NSString stringWithFormat:@"%s: %02d:%02d:%02d", "Pressing Time", minute, second, milli];
    [_txtTimer setText:txt];
}

@end
