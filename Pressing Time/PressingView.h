//
//  PressingView.h
//  Pressing Time
//
//  Created by Luu Nguyen on 5/3/16.
//  Copyright © 2016 Uphave. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PressingViewDelegate <NSObject>

- (void)touched;
- (void)pressed;
- (void)released;

@end

@interface PressingView : UIView

@property (nonatomic, assign) id<PressingViewDelegate> delegate;

@end
