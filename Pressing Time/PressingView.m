//
//  PressingView.m
//  Pressing Time
//
//  Created by Luu Nguyen on 5/3/16.
//  Copyright © 2016 Uphave. All rights reserved.
//

#import "PressingView.h"
#include <stdlib.h>
@import AVFoundation;

@interface PressingView()

@end

@implementation PressingView {
    int _numWaves;
    CGFloat _touchRadius;
    CGPoint _waveCenter;
    CGColorRef _waveColor;
    NSMutableArray *_waveAlpha;
    NSMutableArray *_waveRadius;
    BOOL _touchingDown;
    BOOL _pressed;
    BOOL _waving;
    NSTimer *_repeatTimer;
    int _playingSound;
    AVAudioPlayer *_player1;
    AVAudioPlayer *_player2;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setMultipleTouchEnabled:NO];
        
        _waveCenter.x = _waveCenter.y = 0;
        [self initWave];
        
        NSString *path1 = [[NSBundle mainBundle] pathForResource: @"ding" ofType: @"mp3"];
        NSURL *file1 = [[NSURL alloc] initFileURLWithPath: path1];
        _player1 = [[AVAudioPlayer alloc] initWithContentsOfURL: file1 error: nil];
        _player1.numberOfLoops = -1;
        _player1.currentTime = 0;
        _player1.volume = 1.0;

        NSString *path2 = [[NSBundle mainBundle] pathForResource: @"kick" ofType: @"mp3"];
        NSURL *file2 = [[NSURL alloc] initFileURLWithPath: path2];
        _player2 = [[AVAudioPlayer alloc] initWithContentsOfURL: file2 error: nil];
        _player2.numberOfLoops = 0;
        _player2.currentTime = 0;
        _player2.volume = 1.0;
    }
    return self;
}

- (void)initWave {
    _touchRadius = 50;
    _waveColor = [[UIColor redColor] CGColor];
    _numWaves = 10;
    
    _waveAlpha = [NSMutableArray array];
    _waveRadius = [NSMutableArray array];

    for (int i = 0; i < _numWaves; i++) {
        [_waveRadius addObject:[NSNumber numberWithFloat:_touchRadius]];
        [_waveAlpha addObject: [NSNumber numberWithFloat:1]];
    }

}

int randomNumber(float from, float to) {
    return from + (arc4random() % (int)(to-from+1));
}

- (void)drawRect:(CGRect)rect {
    if(!_waveCenter.x || !_waveCenter.y) {
        _waveCenter.x = rect.size.width / 2;
        _waveCenter.y = rect.size.height / 2;
    }
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextClearRect(ctx, rect);
    
    CGRect circleBounds = CGRectMake(_waveCenter.x - _touchRadius, _waveCenter.y - _touchRadius, 2 * _touchRadius, 2 * _touchRadius);
    CGContextSetFillColor(ctx, CGColorGetComponents(_waveColor));
    CGContextAddEllipseInRect(ctx, circleBounds);
    CGContextFillPath(ctx);

    if (_waving) {
        CGContextSetStrokeColorWithColor(ctx, _waveColor);
        CGContextSetLineWidth(ctx, 1.0);
        for (int i = 0; i < _numWaves; i++) {
            CGContextSetAlpha(ctx, [[_waveAlpha objectAtIndexedSubscript:i] floatValue]);
            float radius = [[_waveRadius objectAtIndexedSubscript:i] floatValue];
            CGContextAddEllipseInRect(ctx, CGRectMake(_waveCenter.x - radius, _waveCenter.y - radius, 2 * radius, 2 * radius));
            CGContextStrokePath(ctx);
        }
        
    }
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];

    if (!_touchingDown) {
        
        if (_delegate) {
            [_delegate touched];
        }
        
        _touchingDown = YES;
        
        CGPoint touchLoc = [touch locationInView:self];
        
        if (pow(_waveCenter.x - touchLoc.x, 2) + pow(_waveCenter.y - touchLoc.y, 2) < pow(_touchRadius, 2)) {
            
            _playingSound = [self playSoundId:0];
            _pressed = YES;
            [self startWave:touchLoc];
            
            if (_delegate) {
                [_delegate pressed];
            }
        }
        else {
            _playingSound = [self playSoundId:1];
        }
        
    }}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (_pressed) {
        UITouch *touch = [touches anyObject];

        CGPoint touchLoc = [touch locationInView:self];
        
        if (pow(_waveCenter.x - touchLoc.x, 2) + pow(_waveCenter.y - touchLoc.y, 2) > 1.5 * pow(_touchRadius, 2)) {
            _pressed = NO;
            
            [self stopSound];
            
            [self stopWave];
            
            _waveCenter.x = randomNumber(_touchRadius, self.frame.size.width - _touchRadius);
            _waveCenter.y = randomNumber(_touchRadius, self.frame.size.height - _touchRadius);
            
            if (_delegate) {
                [_delegate released];
            }
            
            [self setNeedsDisplay];
        }
        
    }
 
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (_touchingDown) {
        _touchingDown = NO;
        
        if (_pressed) {
            _pressed = NO;
            
            [self stopSound];
            
            [self stopWave];
            
            _waveCenter.x = randomNumber(_touchRadius, self.frame.size.width - _touchRadius);
            _waveCenter.y = randomNumber(_touchRadius, self.frame.size.height - _touchRadius);
            
            if (_delegate) {
                [_delegate released];
            }
            
            [self setNeedsDisplay];
        }
    }

}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self touchesEnded:touches withEvent:event];
}

- (void)startWave:(CGPoint)touchLoc {
    _waveCenter = touchLoc;
    _waving = YES;
    [self initWave];
    [_repeatTimer invalidate];
    _repeatTimer = [NSTimer scheduledTimerWithTimeInterval:0.05
                                                      target:self
                                                    selector:@selector(updateWave)
                                                    userInfo:nil repeats:YES];
}

- (void)stopWave {
    if (_waving) {
        _waving = NO;
        [_repeatTimer invalidate];
        [self setNeedsDisplay];
    }
}

- (void)updateWave {
    for (int i = 0; i < _numWaves; i++) {
        CGFloat radius = [[_waveRadius objectAtIndexedSubscript:i] floatValue];
        CGFloat alpha = [[_waveAlpha objectAtIndexedSubscript:i] floatValue];
        
        if (radius == _touchRadius) {
            // check whether initial wave
            int previousWave = (i == 0) ? _numWaves - 1 : i - 1;
            CGFloat previousRadius = [[_waveRadius objectAtIndexedSubscript:previousWave] floatValue];
            if (i != 0 || previousRadius != _touchRadius) {
                if (previousRadius - _touchRadius < _touchRadius / 4) {
                    continue;
                }
            }
        }

        alpha -= 0.023;
        [_waveRadius setObject:[NSNumber numberWithFloat:radius * 1.05f] atIndexedSubscript:i];
        [_waveAlpha setObject:[NSNumber numberWithFloat:alpha] atIndexedSubscript:i];
        
        if (alpha < 0) {
            [_waveRadius setObject:[NSNumber numberWithFloat:_touchRadius] atIndexedSubscript:i];
            [_waveAlpha setObject:[NSNumber numberWithFloat:1] atIndexedSubscript:i];
        }
    }
    
    [self setNeedsDisplay];
}

// TODO: sound

- (int)playSoundId:(int)soundId {

    if(soundId)
        [_player2 play];
    else
        [_player1 play];

    return soundId;
}

- (void)stopSound {
    if(!_playingSound) [_player1 stop];
}

@end
